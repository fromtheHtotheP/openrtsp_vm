#set the base image
FROM ubuntu
#author
MAINTAINER Paul Hx
#extra metadata
LABEL version="1.0"
LABEL description="openrtsp_with_docker"
# update sources list
RUN apt-get clean
RUN apt-get update
# install basic apps
RUN apt-get install -y apt-utils
RUN apt-get install -y livemedia-utils
RUN apt-get install -y smbclient
RUN apt-get install -y cifs-utils 
RUN apt-get install -y git
RUN apt-get install -y open-iscsi
#RUN apt-get install -y open-iscsi-utils

#ssh + keys
RUN apt-get install -y openssh-server
RUN mkdir /var/run/sshd
EXPOSE 2222
RUN cp /root/auth/id_rsa.pub /root/.ssh/authorized_keys
RUN rm -f /root/auth
RUN chmod 700 /root/.ssh
RUN chmod 400 /root/.ssh/authorized_keys
RUN chown root. /root/.ssh/authorized_keys
CMD /usr/sbin/sshd -D

# Setup git 
RUN mkdir -p /root/.ssh 
ADD id_rsa_rtsp_receive /root/.ssh/id_rsa_rtsp_receive
RUN chmod 700 /root/.ssh/id_rsa_receive
RUN echo "Host github.com\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config

# connect iscsi targets
RUN echo "mounting iscsi targets"

# mounts


# get scripts and make dirs
RUN mkdir /root/scripts
RUN git clone https://fromtheHtotheP@bitbucket.org/fromtheHtotheP/openrtsp_vm.git /home/scripts

